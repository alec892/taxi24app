#### Requerimientos

* Docker
* docker-compose
* Make

#### Levantar Ambiente

1.- Colocarse dentro de la carpeta del proyecto taxi 24

2.- Ejecutar el siguiente comando que creara las imagen de docker para el backend y la base de datos

~~~~
$ make build
~~~~

3.- Ejecutar el siguiente comando para levantar el ambiente
~~~~
$ make up
~~~~


