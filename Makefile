.DEFAULT_GOAL := help
.PHONY: venv
.EXPORT_ALL_VARIABLES:

PROJECT_NAME	    = auna-db-taxi24
IMAGE               = auna-dev-taxi24:latest
IMAGE_DB            = auna-db-taxi24:latest
CONTAINER_NAME      = auna-dev-taxi24_backend
CONTAINER_DB_NAME   = mysql_services
DOCKER_NETWORK      = auna_network

build:
	@cp requirements.txt docker/dev/resources/requirements.txt
	@docker build -f docker/dev/Dockerfile --no-cache -t ${IMAGE} docker/dev/
	@docker build -f docker/mysql/Dockerfile --no-cache -t ${IMAGE_DB} docker/mysql/
	@rm -f docker/dev/resources/requirements.txt

up: verify_network
	IMAGE=$(IMAGE) \
	CONTAINER_NAME=$(CONTAINER_NAME) \
	IMAGE_DB=$(IMAGE_DB) \
	CONTAINER_DB_NAME=$(CONTAINER_DB_NAME) \
	DOCKER_NETWORK=$(DOCKER_NETWORK) \
	docker-compose -p $(PROJECT_NAME) up -d
	docker-compose -p $(PROJECT_NAME) ps

down:
	@docker-compose -p $(PROJECT_NAME) down

log:
	@docker logs -f  $(CONTAINER_NAME)

create-migrate:
	docker exec -it $(CONTAINER_NAME) python manage.py makemigrations apptaxi24

migrate:
	docker exec -it $(CONTAINER_NAME) python manage.py migrate

verify_network: ## Verify the local network was created in docker: make verify_network
	@if [ -z $$(docker network ls | grep $(DOCKER_NETWORK) | awk '{print $$2}') ]; then\
		(docker network create $(DOCKER_NETWORK));\
	fi