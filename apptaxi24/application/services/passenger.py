from apptaxi24.domain.service.passenger import PassengerDomainService


class GetOnePassengerApplicationService:

    def __init__(self, domain_service: PassengerDomainService):
        self.domain_service = domain_service

    def execute(self, id):
        return self.domain_service.find_by_id(id)


class GetAllPassengerApplicationService:
    def __init__(self, domain_service: PassengerDomainService):
        self.domain_service = domain_service

    def execute(self):
        return self.domain_service.get_all()
