from apptaxi24.domain.service.trip import TripDomainService
from apptaxi24.domain.service.driver import DriverDomainService
from apptaxi24.domain.entities.trip import Trip
from apptaxi24.domain.enum.state import StateEnum
from datetime import datetime
import random


class AssignedTripApplicationService:

    def __init__(
        self,
        trip_domain_service: TripDomainService,
        driver_domain_service: DriverDomainService
    ):
        self.trip_domain_service = trip_domain_service
        self.driver_domain_service = driver_domain_service

    def execute(self, schema_trip):
        filters = {'available': 1}
        drivers = self.driver_domain_service.get_all(**filters)
        drive = random.choice(drivers)

        trip = Trip(**schema_trip)
        trip.created_at = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.now())
        trip.driver_id = drive
        trip.state = StateEnum.ASSIGNED
        return self.trip_domain_service.save(trip)


class GetAllTripApplicationService:
    def __init__(self, domain_service: TripDomainService):
        self.domain_service = domain_service

    def execute(self, **filters):
        return self.domain_service.get_all(**filters)


class CompleteTripApplicationService:
    def __init__(self, domain_service: TripDomainService):
        self.domain_service = domain_service

    def execute(self, id, data):
        trip = Trip(id=id, state=data['state'])
        return self.domain_service.update_state(trip)
