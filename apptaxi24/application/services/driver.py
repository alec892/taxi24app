from apptaxi24.domain.service.driver import DriverDomainService


class GetOneDriverApplicationService:

    def __init__(self, domain_service: DriverDomainService):
        self.domain_service = domain_service

    def execute(self, id):
        return self.domain_service.find_by_id(id)


class GetAllDriverApplicationService:

    def __init__(self, domain_service: DriverDomainService):
        self.domain_service = domain_service

    def execute(self, **filters):
        return self.domain_service.get_all(**filters)
