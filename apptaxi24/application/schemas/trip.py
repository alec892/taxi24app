from marshmallow import Schema, fields, pre_load


class TripSchema(Schema):

    passenger_id = fields.Integer(required=True)
    origin = fields.Str(required=True)
    destination = fields.Str(required=True)


class TripFilterSchema(Schema):

    state = fields.Str(required=False)

    @pre_load
    def get_values(self, data):
        tmp_data = {}
        if 'state' in data:
            tmp_data['state'] = data['state']

        return tmp_data


class TripStateSchema(Schema):

    state = fields.Str(required=True)
