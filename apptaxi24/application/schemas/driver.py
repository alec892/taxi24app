from marshmallow import Schema, fields, pre_load


class DriverSchema(Schema):

    available = fields.Integer(required=False)

    @pre_load
    def get_values(self, data):
        if 'available' in data:
            data['available'] = data['available'][0]
        return data

