from django.views import View
from django.http import JsonResponse

from apptaxi24.infraestructure.dependency_injection.injector import AppServicesInjector


class PassengerGetOneHandler(View):

    def get(self, request, id):
        passenger_service = AppServicesInjector.get_one_passenger()
        data = passenger_service.execute(id)

        return JsonResponse({'message': 'success', 'data': data.__dict__})


class PassengerGetAllHandler(View):

    def get(self, request):
        passenger_service = AppServicesInjector.get_all_passenger()
        data = passenger_service.execute()
        data_list = [passenger.__dict__ for passenger in data]

        return JsonResponse({'message': 'success', 'data': data_list})
