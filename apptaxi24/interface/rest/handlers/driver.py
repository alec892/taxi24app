from django.views import View
from django.http import JsonResponse, request

from apptaxi24.infraestructure.dependency_injection.injector import AppServicesInjector
from apptaxi24.application.schemas.driver import DriverSchema


class DriverGetOneHandler(View):

    def get(self, req: request.HttpRequest, id):
        driver_service = AppServicesInjector.get_one_driver()
        data = driver_service.execute(id)

        return JsonResponse({'message': 'success', 'data': data.__dict__})


class DriverGetAllHandler(View):

    def get(self, req: request.HttpRequest):

        schema = DriverSchema()
        params = req.GET.copy()
        filters = schema.load(params)

        driver_service = AppServicesInjector.get_all_driver()
        data = driver_service.execute(**filters.data)

        data_list = [driver.__dict__ for driver in data]
        return JsonResponse({'message': 'success', 'data': data_list})

