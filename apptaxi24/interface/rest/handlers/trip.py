from django.views import View
from django.http import JsonResponse, request
from apptaxi24.infraestructure.dependency_injection.injector import AppServicesInjector
from apptaxi24.application.schemas.trip import TripSchema, TripStateSchema, TripFilterSchema
import json


class TripHandler(View):
    def get(self, req: request.HttpRequest):

        schema = TripFilterSchema()
        filters = schema.load(req.GET)

        trip_service = AppServicesInjector.get_all_trip()
        data = trip_service.execute(**filters.data)
        data_list = [trip.__dict__ for trip in data]

        return JsonResponse({'message': 'success', 'data': data_list})

    def post(self, req: request.HttpRequest):
        schema = TripSchema()
        schema_data = schema.load(json.loads(req.body))

        trip_service = AppServicesInjector.assigned_trip()
        data = trip_service.execute(schema_data.data)

        return JsonResponse({'message': 'success', 'data': data.__dict__})


class OneTripHandler(View):
    def patch(self, req: request.HttpRequest, id):
        schema = TripStateSchema()

        schema_data = schema.load(json.loads(req.body))

        trip_service = AppServicesInjector.complete_trip()
        data = trip_service.execute(id, schema_data.data)

        return JsonResponse({'message': 'success', 'data': data.__dict__})
