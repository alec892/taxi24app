from django.apps import AppConfig


class Apptaxi24Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apptaxi24'
