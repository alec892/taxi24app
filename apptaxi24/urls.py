from django.urls import path
from apptaxi24.interface.rest.handlers.driver import DriverGetOneHandler, DriverGetAllHandler
from apptaxi24.interface.rest.handlers.passenger import PassengerGetOneHandler, PassengerGetAllHandler
from apptaxi24.interface.rest.handlers.trip import TripHandler, OneTripHandler

urlpatterns = [
    path('drivers', DriverGetAllHandler.as_view(), name='drivers'),
    path('drivers/<int:id>', DriverGetOneHandler.as_view(), name='driver'),
    path('passengers/', PassengerGetAllHandler.as_view(), name='passengers'),
    path('passengers/<int:id>', PassengerGetOneHandler.as_view(), name='passenger'),
    path('trips', TripHandler.as_view(), name='trips'),
    path('trips/<int:id>', OneTripHandler.as_view(), name='trip'),
]
