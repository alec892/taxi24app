

class StateEnum:
    ASSIGNED = 'asignado'
    INPROGRESS = 'vigente'
    FINISHED = 'terminado'
    CANCELLED = 'cancelado'

    state_choices = (
        (ASSIGNED, ASSIGNED),
        (INPROGRESS, INPROGRESS),
        (FINISHED, FINISHED),
        (CANCELLED, CANCELLED)
    )
