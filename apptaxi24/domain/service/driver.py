from apptaxi24.domain.repository.driver import DriverRepository


class DriverDomainService:

    def __init__(self, driver_repository: DriverRepository):
        self.driver_repository = driver_repository

    def find_by_id(self, id):
        return self.driver_repository.find_by_id(id)

    def get_all(self, **filters):
        return self.driver_repository.get_all(**filters)
