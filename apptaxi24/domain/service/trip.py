from apptaxi24.domain.repository.trip import TripRepository
from apptaxi24.domain.entities.trip import Trip


class TripDomainService:

    def __init__(self, trip_repository: TripRepository):
        self.trip_repository = trip_repository

    def save(self, trip: Trip):
        return self.trip_repository.save(trip)

    def update_state(self, trip: Trip):
        return self.trip_repository.update_state(trip)

    def get_all(self, **filters):
        return self.trip_repository.get_all(**filters)
