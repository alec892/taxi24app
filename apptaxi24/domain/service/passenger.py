from apptaxi24.domain.repository.passenger import PassengerRepository


class PassengerDomainService:

    def __init__(self, passenger_repository: PassengerRepository):
        self.passenger_repository = passenger_repository

    def find_by_id(self, id):
        return self.passenger_repository.find_by_id(id)

    def get_all(self):
        return self.passenger_repository.get_all()
