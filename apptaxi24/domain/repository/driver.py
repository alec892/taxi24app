from abc import ABC, abstractmethod


class DriverRepository(ABC):

    @abstractmethod
    def get_all(self, **filters):
        raise NotImplementedError

    @abstractmethod
    def find_by_id(self, id):
        raise NotImplementedError
