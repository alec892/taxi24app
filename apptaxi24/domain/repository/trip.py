from abc import ABC, abstractmethod
from apptaxi24.domain.entities.trip import Trip


class TripRepository(ABC):

    @abstractmethod
    def save(self, trip: Trip):
        raise NotImplementedError

    def update_state(self, trip: Trip):
        raise NotImplementedError

    def get_all(self, **filters):
        raise NotImplementedError

