from abc import ABC, abstractmethod


class PassengerRepository(ABC):

    @abstractmethod
    def find_by_id(self, id):
        raise NotImplementedError

    @abstractmethod
    def get_all(self):
        raise NotImplementedError
