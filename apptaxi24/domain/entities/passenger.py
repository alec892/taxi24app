

class Passenger:

    def __init__(
            self,
            id: None,
            name: None,
            lastname: None,
            cellphone: None,
            state: None
    ):
        self.id = id
        self.name = name
        self.lastname = lastname
        self.cellphone = cellphone
        self.state = state
