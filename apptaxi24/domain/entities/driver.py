

class Driver:

    def __init__(
            self,
            id: None,
            name: None,
            lastname: None,
            available: None,
            state: None
    ):
        self.id = id
        self.name = name
        self.lastname = lastname
        self.available = available
        self.state = state
