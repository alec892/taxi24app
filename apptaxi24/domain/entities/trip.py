

class Trip:

    def __init__(
            self,
            id=None,
            driver_id=None,
            passenger_id=None,
            origin=None,
            destination=None,
            created_at=None,
            arrive_time=None,
            state=None
    ):
        self.id = id
        self.driver_id = driver_id
        self.passenger_id = passenger_id
        self.origin = origin
        self.destination = destination
        self.created_at = created_at
        self.arrive_time = arrive_time
        self.state = state
