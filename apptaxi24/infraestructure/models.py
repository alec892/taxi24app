from django.db import models
from apptaxi24.domain.enum.state import StateEnum
from apptaxi24.domain.enum.document_type import DocumentTypeEnum


class Driver(models.Model):

    class DocumentType(models.TextChoices):
        DNI = DocumentTypeEnum.DNI
        IMMIGRATION_CARD = DocumentTypeEnum.IMMIGRATION_CARD
        PASSPORT = DocumentTypeEnum.PASSPORT

    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    document_type = models.CharField(max_length=50, choices=DocumentType.choices, default=DocumentType.DNI)
    document_number = models.CharField(max_length=50)
    cellphone = models.CharField(max_length=9)
    available = models.BooleanField()
    state = models.IntegerField(default=1)


class Passenger(models.Model):
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    cellphone = models.CharField(max_length=9)
    state = models.IntegerField(default=1)


class Trip(models.Model):

    class State(models.TextChoices):
        ASSIGNED = StateEnum.ASSIGNED
        INPROGRESS = StateEnum.INPROGRESS
        FINISHED = StateEnum.FINISHED
        CANCELLED = StateEnum.CANCELLED

    driver_id = models.ForeignKey(Driver, on_delete=models.CASCADE)
    passenger_id = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    origin = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    created_at = models.DateTimeField()
    arrive_time = models.DateTimeField(null=True)
    state = models.CharField(max_length=50, choices=State.choices, default=State.ASSIGNED)
