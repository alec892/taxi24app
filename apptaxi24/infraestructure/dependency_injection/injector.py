import dependency_injector.containers as containers
import dependency_injector.providers as providers

from apptaxi24.infraestructure.repository.driver import DriverMysqlRepository
from apptaxi24.infraestructure.repository.passenger import PassengerMysqlRepository
from apptaxi24.infraestructure.repository.trip import TripMysqlRepository
from apptaxi24.domain.service.driver import DriverDomainService
from apptaxi24.domain.service.passenger import PassengerDomainService
from apptaxi24.domain.service.trip import TripDomainService
from apptaxi24.application.services.driver import GetAllDriverApplicationService, GetOneDriverApplicationService
from apptaxi24.application.services.passenger import GetAllPassengerApplicationService, \
    GetOnePassengerApplicationService
from apptaxi24.application.services.trip import AssignedTripApplicationService, GetAllTripApplicationService, \
    CompleteTripApplicationService



class RepositoryInjector(containers.DeclarativeContainer):

    driver = providers.Singleton(DriverMysqlRepository)
    passenger = providers.Singleton(PassengerMysqlRepository)
    trip = providers.Singleton(TripMysqlRepository)


class DomainServiceInjector(containers.DeclarativeContainer):

    driver = providers.Singleton(
        DriverDomainService,
        driver_repository=RepositoryInjector.driver
    )

    passenger = providers.Singleton(
        PassengerDomainService,
        passenger_repository=RepositoryInjector.passenger
    )

    trip = providers.Singleton(
        TripDomainService,
        trip_repository=RepositoryInjector.trip
    )


class AppServicesInjector(containers.DeclarativeContainer):

    get_one_driver = providers.Singleton(
        GetOneDriverApplicationService,
        domain_service=DomainServiceInjector.driver
    )

    get_all_driver = providers.Singleton(
        GetAllDriverApplicationService,
        domain_service=DomainServiceInjector.driver
    )

    get_one_passenger = providers.Singleton(
        GetOnePassengerApplicationService,
        domain_service=DomainServiceInjector.passenger
    )

    get_all_passenger = providers.Singleton(
        GetAllPassengerApplicationService,
        domain_service=DomainServiceInjector.passenger
    )

    assigned_trip = providers.Singleton(
        AssignedTripApplicationService,
        trip_domain_service=DomainServiceInjector.trip,
        driver_domain_service=DomainServiceInjector.driver
    )

    get_all_trip = providers.Singleton(
        GetAllTripApplicationService,
        domain_service=DomainServiceInjector.trip
    )

    complete_trip = providers.Singleton(
        CompleteTripApplicationService,
        domain_service=DomainServiceInjector.trip
    )
