from apptaxi24.domain.entities.trip import Trip as TripEntity
from apptaxi24.domain.repository.trip import TripRepository
from apptaxi24.models import Trip, Driver, Passenger
from datetime import datetime


class TripMysqlRepository(TripRepository):

    def save(self, trip: TripEntity):
        trip.driver_id = Driver(**trip.driver_id.__dict__)
        trip.passenger_id = Passenger(id=trip.passenger_id)
        trip_model = Trip(**trip.__dict__)
        trip_model.save()
        trip.id = trip_model.id
        trip.driver_id = trip.driver_id.id
        trip.passenger_id = trip.passenger_id.id
        return trip

    def update_state(self, trip: TripEntity):
        arrive_time = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.now())
        Trip.objects.filter(id=trip.id).update(state=trip.state, arrive_time=arrive_time)
        obj = Trip.objects.get(id=trip.id)
        return TripEntity(
            id=obj.id,
            driver_id=obj.driver_id.id,
            passenger_id=obj.passenger_id.id,
            origin=obj.origin,
            destination=obj.destination,
            created_at=obj.created_at,
            arrive_time=obj.arrive_time,
            state=obj.state
        )

    def get_all(self, **filters):
        trips = Trip.objects.all().filter(**filters)
        trips_list = []
        for trip in trips:
            trips_list.append(
                TripEntity(
                    id=trip.id,
                    driver_id=trip.driver_id.id,
                    passenger_id=trip.passenger_id.id,
                    origin=trip.origin,
                    destination=trip.destination,
                    created_at=trip.created_at,
                    arrive_time=trip.arrive_time,
                    state=trip.state
                )
            )
        return trips_list
