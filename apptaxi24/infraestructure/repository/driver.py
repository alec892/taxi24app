from apptaxi24.domain.repository.driver import DriverRepository
from apptaxi24.domain.entities.driver import Driver as DriverEntity
from apptaxi24.models import Driver


class DriverMysqlRepository(DriverRepository):
    def get_all(self, **filters):
        drivers = Driver.objects.all().filter(**filters)
        drivers_list = []
        for drive_model in drivers:
            drivers_list.append(DriverEntity(
                id=drive_model.id,
                name=drive_model.name,
                lastname=drive_model.lastname,
                available=drive_model.available,
                state=drive_model.state
            ))
        return drivers_list

    def find_by_id(self, id):
        drive_model = Driver.objects.get(id=id)
        return DriverEntity(
            id=drive_model.id,
            name=drive_model.name,
            lastname=drive_model.lastname,
            available=drive_model.available,
            state=drive_model.state
        )
