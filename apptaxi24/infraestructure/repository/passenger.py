from apptaxi24.domain.repository.passenger import PassengerRepository
from apptaxi24.models import Passenger
from apptaxi24.domain.entities.passenger import Passenger as PassengerEntity


class PassengerMysqlRepository(PassengerRepository):

    def find_by_id(self, id):
        passenger_model = Passenger.objects.get(id=id)
        return PassengerEntity(
            id=passenger_model.id,
            name=passenger_model.name,
            lastname=passenger_model.lastname,
            cellphone=passenger_model.cellphone,
            state=passenger_model.state
        )

    def get_all(self):
        passengers = Passenger.objects.all()
        passengers_list = []
        for passenger in passengers:
            passengers_list.append(PassengerEntity(
                id=passenger.id,
                name=passenger.name,
                lastname=passenger.lastname,
                cellphone=passenger.cellphone,
                state=passenger.state
            ))
        return passengers_list
